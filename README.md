|           | a==b | a===b | compare (a,b) |
|-----------|------|-------|---------------|
| a=1 , b=1 | true | true  |    true       |
| a=1 , b=new Number(1) | true | false  |    false       |
| a=new Number(1) , b=1 | true | false  |    false       |
| a=new Number(1) ,<br/> b=new Number(1) | false | false  |    true       |
