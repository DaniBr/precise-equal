/** no-op function - just for typescript generic */
export function datatype <C extends ClassConstructor, SameAs extends ClassConstructor> (dt:Datatype<C,SameAs>) {
    return dt
}

/* types */
import type { PreciseEqual } from "./index"
import type { ClassConstructor } from "./handy-types"

type Helper = {
    next: typeof PreciseEqual.compare
    skipUndefined: boolean
}

export type Datatype<C extends ClassConstructor, SameAs extends ClassConstructor = any> = {
    class: C
    compare: (a:InstanceType<C>, b:InstanceType<C>, helper:Helper) => boolean
    overwriteExisting?: boolean
} | DatatypeSameAs <C, SameAs>

type DatatypeSameAs <C1 extends ClassConstructor, C2 extends ClassConstructor> = InstanceType<C1> extends InstanceType<C2> ? {
    class: C1
    sameAs: C2
} : never


/*
class MyDate extends Date {
    rr(m:number) {}
}

datatype ({
    class: MyDate,
    sameAs: Date
})

datatype ({
    class: Date,
    sameAs: MyDate
})

datatype ({
    class: RegExp,
    sameAs: Date
})

*/