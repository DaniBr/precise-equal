import { datatype } from "./datatype"

export const crucialDatatypes = [
    datatype ({ class: Array, compare (a,b, {next, skipUndefined}) {
        if (a.length != b.length) return false
        let len = a.length
        for (let i = 0; i < len; i++) {
            const eql = next (a[i],b[i])
            if (!eql) return false
            if (skipUndefined) continue

            // prevent empty slot from being equal to undefined, e.g.: PreciseEqual.compare ([undefined], [,])
            if (a[i] !== undefined) continue
            if (i in a != i in b) return false
        }
        return true
    }}),
    datatype ({ class: Object, compare (a,b, {next, skipUndefined}) {
        const aKeys = Object.keys(a)
        const bKeys = Object.keys(a)
        if (!skipUndefined && aKeys.length != bKeys.length) return false


    }}),

    // distinct between non-existing property and undefined property
] satisfies Datatype<any>[]

/* types */
import type { Datatype } from "./datatype"

/* see:
https://github.com/planttheidea/fast-equals
https://github.com/lukeed/dequal
https://github.com/epoberezkin/fast-deep-equal
 */