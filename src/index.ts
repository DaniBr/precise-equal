export default PreciseEqual
export class PreciseEqual {
    public readonly skipSymbols = true
    public readonly skipUndefined
    public readonly loadDefaultTypes

    constructor(options:{
        skipUndefined?: boolean
        loadDefaultTypes?: boolean
    }={}) {
        this.skipUndefined = options.skipUndefined ?? false
        this.loadDefaultTypes = options.loadDefaultTypes ?? true
    }

    compare (a:unknown, b:unknown):boolean {
        if (a===b) return true
        if (!(a instanceof Object && b instanceof Object)) return false
        if (!getCommonDenominator(a,b)) return false
        return false
    }

    static default = new PreciseEqual()
    static compare = PreciseEqual.default.compare.bind (PreciseEqual.default) as typeof this.default.compare
/*
    static isSupported () {
        return Boolean (
            Object.entries
        )
    }
*/
}

function getCommonDenominator (a: object, b:object): ClassConstructor | undefined {
    if (a.constructor == b.constructor) return a.constructor as ClassConstructor
    // check sameAs
}

/* types */
import type { ClassConstructor } from "./handy-types"