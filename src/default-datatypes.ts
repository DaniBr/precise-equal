/// <reference lib="ES2020.BigInt" />
import { datatype } from "./datatype"

export const defaultDatatypes = [
    datatype ({ class: Date, compare: (a,b) => a.getTime() == b.getTime() }),
    datatype ({ class: RegExp, compare: (a,b) => a.source == b.source && a.flags == b.flags }),
    datatype ({ class: Number, compare: (a,b) => a.valueOf() == b.valueOf() }),
    datatype ({ class: String, compare: (a,b) => a.valueOf() == b.valueOf() }),
    datatype ({ class: Boolean, compare: (a,b) => a.valueOf() == b.valueOf() }),
    ...[
        Int8Array,
        Uint8ClampedArray,
        Int16Array,
        Uint16Array,
        Int32Array,
        Uint32Array,
        Float32Array,
        Float64Array,
        ... typeof BigInt == "function" ? [
            BigInt64Array,
            BigUint64Array,
        ] : [/* empty */],
    ].map ( typedArray => datatype ({
        class: typedArray,
        compare: (a, b) => a.length == b.length && a.every( (item,idx) => item==b[idx] ),
    })),
    // Map
    // Set
] satisfies Datatype<any>[]

/* types */
import type { Datatype } from "./datatype"